import Vue from 'vue'
import App from './App.vue'
import './assets/js/draggablePanel.js'

Vue.config.productionTip = false

new Vue({
    render: h => h(App),
}).$mount('#app')

new Vue({
    el: '#dragging',
    data: () => ({
        dividerPosition: 50
    }),
    methods: {
        handleDragging(e) {
            const percentage = (e.pageX / window.innerWidth) * 100

            if (percentage >= 10 && percentage <= 90) {
                this.dividerPosition = percentage.toFixed(2)
            }
        },
        startDragging() {
            document.addEventListener('mousemove', this.handleDragging)
        },
        endDragging() {
            document.removeEventListener('mousemove', this.handleDragging)
        },
    }
})